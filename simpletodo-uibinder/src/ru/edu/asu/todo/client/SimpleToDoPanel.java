package ru.edu.asu.todo.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class SimpleToDoPanel extends Composite {

	private static SimpleToDoPanelUiBinder uiBinder = GWT.create(SimpleToDoPanelUiBinder.class);

	interface SimpleToDoPanelUiBinder extends UiBinder<Widget, SimpleToDoPanel> {
	}

	private static final int COLUMN_DATE = 0;
	private static final int COLUMN_CONTENT = 1;
	private static final int COLUMN_CHECK_BOX = 2;
	
	private List<ToDo> todos = new ArrayList<>();

	@UiField Grid toDoGrid;
	@UiField VerticalPanel mainPanel;
	@UiField HorizontalPanel editPanel;
	@UiField TextBox editToDoTextBox;
	@UiField CheckBox completeToDoCheckBox; 
	@UiField Button saveToDoButton;
	@UiField Label totalLabel;

	private ToDo selectedToDo = null;

	
	public SimpleToDoPanel() {
		initWidget(uiBinder.createAndBindUi(this));

		todos.add(new ToDo("Build a house"));
		todos.add(new ToDo("Write a song"));
		todos.add(new ToDo("See Star Wars"));

		toDoGrid.getColumnFormatter().setWidth(COLUMN_DATE, "30%");
		toDoGrid.getColumnFormatter().setWidth(COLUMN_CONTENT, "50%");
		toDoGrid.getColumnFormatter().setWidth(COLUMN_CHECK_BOX, "20%");
		fillGrid();
		updateTotal();
	
	}

	@UiHandler("saveToDoButton")
	public void onSaveButtonClick(ClickEvent event) {
		saveToDo();
	}
	
	@UiHandler("toDoGrid")
	public void onToDoGridClick(ClickEvent event) {
		Cell cellForEvent = toDoGrid.getCellForEvent(event);
		if (cellForEvent != null) {
			selectToDo(cellForEvent.getRowIndex() - 1);
		}
	}

	private void updateTotal() {
		totalLabel.setText("Total ToDo's: " + todos.size()); 
	}

	private void saveToDo() {
		String content = editToDoTextBox.getValue();
		if ((content == null) ||  (content.isEmpty())) {
			displayError();
			return;
		}
		if (selectedToDo == null) {
			selectedToDo = new ToDo(content);
			addToDo(selectedToDo);
		} else {
			selectedToDo.setContent(content);
			selectedToDo.setCompleted(completeToDoCheckBox.getValue());
			updateToDo(selectedToDo);
		}
		clearSelectedToDo();
	}


	private void displayError() {
		Window.alert("Enter To Do text!");
		
	}

	private void clearSelectedToDo() {
		editToDoTextBox.setText("");
		completeToDoCheckBox.setValue(false);
		selectedToDo = null;
	}

	private void addToDo(ToDo selectedToDo) {
		todos.add(selectedToDo);
		addToDoToGrid(selectedToDo);
		updateTotal();
	}
	
	private void updateToDo(ToDo selectedToDo) {
		int index = todos.indexOf(selectedToDo) + 1;
		if ((index == 0) || (index == 1)) return;
		toDoGrid.setText(index, COLUMN_CONTENT, selectedToDo.getContent());
		CheckBox completedCheckBox = (CheckBox)toDoGrid.getWidget(index, COLUMN_CHECK_BOX);
		completedCheckBox.setValue(selectedToDo.isCompleted());
	}
	
	private void addToDoToGrid(ToDo todo) {
		int row = toDoGrid.getRowCount();
		toDoGrid.insertRow(toDoGrid.getRowCount());
		String dateString = DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_SHORT).format(todo.getDate()); 
		toDoGrid.setText(row, COLUMN_DATE, dateString);
		toDoGrid.setText(row, COLUMN_CONTENT, todo.getContent());
		CheckBox completedCheckBox = new CheckBox();
		completedCheckBox.setEnabled(false);
		completedCheckBox.setValue(todo.isCompleted());
		toDoGrid.setWidget(row, COLUMN_CHECK_BOX, completedCheckBox);
	}
	
	private void fillGrid() {
		for (ToDo todo: todos) {
			addToDoToGrid(todo);
		}
	}
	
	private void selectToDo(int index) {
		selectedToDo = todos.get(index);
		editToDoTextBox.setValue(selectedToDo.getContent());
		completeToDoCheckBox.setValue(selectedToDo.isCompleted());
	}

}
