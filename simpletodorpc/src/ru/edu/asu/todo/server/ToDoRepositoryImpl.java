package ru.edu.asu.todo.server;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import ru.edu.asu.todo.client.ToDoRepository;
import ru.edu.asu.todo.shared.ToDo;

public class ToDoRepositoryImpl extends RemoteServiceServlet implements ToDoRepository {

	private List<ToDo> todos = new ArrayList<>();
	private long lastId = 0;
	
	public ToDoRepositoryImpl() {
		super();
		addToDo(new ToDo("Build a house"));
		addToDo(new ToDo("Write a song"));
		addToDo(new ToDo("See Star Wars"));
	}

	@Override
	public List<ToDo> getAllToDos() {
		return todos;
	}

	@Override
	public ToDo addToDo(ToDo todo) {
		todo.setId(lastId);
		lastId++;
		todos.add(todo);
		return todo;
	}

	@Override
	public void updateToDo(ToDo todo) {
		for (int i = 0; i < todos.size(); i++) {
			if (todos.get(i).getId() == todo.getId()) {
				todos.set(i, todo);
				break;
			}
		}
	}

}
