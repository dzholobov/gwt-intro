package ru.edu.asu.todo.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Simpletodorpc implements EntryPoint {

	public void onModuleLoad() {
		SimpleToDoPanel toDoPanel = new SimpleToDoPanel();
		RootPanel.get().add(toDoPanel);

	}
	
	
}
