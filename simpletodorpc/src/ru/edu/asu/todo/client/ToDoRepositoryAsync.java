package ru.edu.asu.todo.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

import ru.edu.asu.todo.shared.ToDo;

public interface ToDoRepositoryAsync {

	void addToDo(ToDo todo, AsyncCallback<ToDo> callback);

	void getAllToDos(AsyncCallback<List<ToDo>> callback);

	void updateToDo(ToDo todo, AsyncCallback<Void> callback);

}
