package ru.edu.asu.todo.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import ru.edu.asu.todo.shared.ToDo;

@RemoteServiceRelativePath("todoService")
public interface ToDoRepository extends RemoteService {

	public List<ToDo> getAllToDos();
	public ToDo addToDo(ToDo todo);
	public void updateToDo(ToDo todo);
	
}
