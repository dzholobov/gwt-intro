package ru.edu.asu.todo.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DateTimeFormat.PredefinedFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Simpletodo implements EntryPoint {

	private static final int COLUMN_DATE = 0;
	private static final int COLUMN_CONTENT = 1;
	private static final int COLUMN_CHECK_BOX = 2;
	
	private List<ToDo> todos = new ArrayList<>();

	private Grid toDoGrid;
	private VerticalPanel mainPanel;
	private HorizontalPanel editPanel;
	private TextBox editToDoTextBox;
	private CheckBox completeToDoCheckBox; 
	private Button saveToDoButton;
	private Label totalLabel;

	private ToDo selectedToDo = null;
	
	public Simpletodo() {
		todos.add(new ToDo("Build a house"));
		todos.add(new ToDo("Write a song"));
		todos.add(new ToDo("See Star Wars"));
	}
	
	public void onModuleLoad() {
		toDoGrid = new Grid(1, 3);
		toDoGrid.addStyleName("toDoGrid");
		toDoGrid.setTitle("To Do List");
		toDoGrid.setText(0, COLUMN_DATE, "Date");
		toDoGrid.setText(0, COLUMN_CONTENT, "To Do");
		toDoGrid.setText(0, COLUMN_CHECK_BOX, "Completed");
		toDoGrid.setWidth("100%");
		toDoGrid.getColumnFormatter().setWidth(COLUMN_DATE, "30%");
		toDoGrid.getColumnFormatter().setWidth(COLUMN_CONTENT, "50%");
		toDoGrid.getColumnFormatter().setWidth(COLUMN_CHECK_BOX, "20%");
		toDoGrid.getRowFormatter().addStyleName(0, "toDoGridHeader");
		fillGrid();

		mainPanel = new VerticalPanel();
		mainPanel.setWidth("90%");
		mainPanel.add(toDoGrid);

		totalLabel = new Label();
		totalLabel.addStyleName("bold");
		mainPanel.add(totalLabel);
		updateTotal();

		editToDoTextBox = new TextBox();
		completeToDoCheckBox = new CheckBox("Complete");
		saveToDoButton = new Button("Save");
		
		
		editPanel = new HorizontalPanel();
		editPanel.addStyleName("editToDoPanel");
		editPanel.add(editToDoTextBox);
		editPanel.add(completeToDoCheckBox);
		editPanel.add(saveToDoButton);

		mainPanel.add(editPanel);

		RootPanel.get().add(mainPanel);

		saveToDoButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				saveToDo();
			}
		});
		
		toDoGrid.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Cell cellForEvent = toDoGrid.getCellForEvent(event);
				if (cellForEvent != null) {
					selectToDo(cellForEvent.getRowIndex() - 1);
				}
			}
		});
	}
	
	
	private void updateTotal() {
		totalLabel.setText("Total ToDo's: " + todos.size()); 
	}

	private void saveToDo() {
		String content = editToDoTextBox.getValue();
		if ((content == null) ||  (content.isEmpty())) {
			displayError();
			return;
		}
		if (selectedToDo == null) {
			selectedToDo = new ToDo(content);
			addToDo(selectedToDo);
		} else {
			selectedToDo.setContent(content);
			selectedToDo.setCompleted(completeToDoCheckBox.getValue());
			updateToDo(selectedToDo);
		}
		clearSelectedToDo();
	}


	private void displayError() {
		Window.alert("Enter To Do text!");
		
	}

	private void clearSelectedToDo() {
		editToDoTextBox.setText("");
		completeToDoCheckBox.setValue(false);
		selectedToDo = null;
	}

	private void addToDo(ToDo selectedToDo) {
		todos.add(selectedToDo);
		addToDoToGrid(selectedToDo);
		updateTotal();
	}
	
	private void updateToDo(ToDo selectedToDo) {
		int index = todos.indexOf(selectedToDo) + 1;
		if ((index == 0) || (index == 1)) return;
		toDoGrid.setText(index, COLUMN_CONTENT, selectedToDo.getContent());
		CheckBox completedCheckBox = (CheckBox)toDoGrid.getWidget(index, COLUMN_CHECK_BOX);
		completedCheckBox.setValue(selectedToDo.isCompleted());
	}
	
	private void addToDoToGrid(ToDo todo) {
		int row = toDoGrid.getRowCount();
		toDoGrid.insertRow(toDoGrid.getRowCount());
		String dateString = DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_SHORT).format(todo.getDate()); 
		toDoGrid.setText(row, COLUMN_DATE, dateString);
		toDoGrid.setText(row, COLUMN_CONTENT, todo.getContent());
		CheckBox completedCheckBox = new CheckBox();
		completedCheckBox.setEnabled(false);
		completedCheckBox.setValue(todo.isCompleted());
		toDoGrid.setWidget(row, COLUMN_CHECK_BOX, completedCheckBox);
	}
	
	private void fillGrid() {
		for (ToDo todo: todos) {
			addToDoToGrid(todo);
		}
	}
	
	private void selectToDo(int index) {
		selectedToDo = todos.get(index);
		editToDoTextBox.setValue(selectedToDo.getContent());
		completeToDoCheckBox.setValue(selectedToDo.isCompleted());
	}

}
